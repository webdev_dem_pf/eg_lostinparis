const player_data =document.querySelectorAll('[data-player]');
const playerObject =Array.from(player_data).map(item => JSON.parse(item.dataset.player));

// creation de la carte
var centerview=[playerObject[0].latitude,playerObject[0].longitude]; 
document.getElementById("playerInterface").style="width: 24%;"
document.getElementById("mapid").style="width: 75%;";
var mymap = L.map('mapid').setView([centerview[0], centerview[1]], 15);
L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(mymap);
// declaration des tableaux contenant les Urls, les Positions et le niveau de zoom maximum des icones
var IconURLs = ['1.png', '2.png', '3.png', '4.png', '5.png', '6.png', '7.png', '8.png', '9.png', '10.png'];
var Positions = [[ 48.85849625335568, 2.294450393617204 ],[ 48.873996319362, 2.2950382263365 ],[ 48.853088190736, 2.3498806398297 ],[ 48.833932933443, 2.3323045912125 ],[ 48.842293221759, 2.3219943128412 ],[ 48.860738130825, 2.3377083705126 ],[ 48.861634320068, 2.393483226336 ],[ 48.850066317762, 2.2797002155883 ],[ 48.845714597751, 2.4247284208912 ],[playerObject[0].latitude,playerObject[0].longitude]];
var Zooms = [17, 17, 18, 17, 18, 17, 18, 17, 17, 1];
var d = ["La foudre est l’arme et l’attribut du dieu céleste.",
"La lumière de l’aurore, symbole ancestrale de feu, éclaire les ténèbres.",
"Le sceau de la destruction est invoque pour destruction.",
"Orcus, symbole ancestrale de ténèbres, est le gardien du portail dimensionnel qui lie le monde des vivants a celui des morts.",
"Le trou de verre, symbole ancestrale de l’espace, est le portail vers les mondes parallèles.",
"La légende dit que derrière le masque démoniaque se cache l’esprit du Fondateur universel.",
"Le sceau mortel, symbole ancestrale de mortalité, est la porte vers le monde des morts.",
"Le cristal céleste d’une pureté incomparable, garde la mémoire de tout être.",
"Carte IGN"];
var I = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

/*
var IconURLs=[];
var Positions =[];
var Zooms =[];
var d =[];
var I =[];
$(document).ready(function(event){
    $.ajax({
        url: "/game_items",
        type: "POST",
        data: {"valid": 1},
        dataType: "json",
        async: true,
        success: function(data,status){
            console.log(data);
            for(let i=8;i>0;i--){
                I.push(data[i].id);
                Zooms.push(1);//data[i].visibility);
                d.push(data[i].description);
                Positions.push([data[i].latitude,data[i].longitude]);
                IconURLs.push(data[i].id+'.png');
            }
            console.log(Positions);
        }
    })
});
setTimeout(() => { console.log(); }, 100);
*/

// creation de l'objet LeafIcon
var LeafIcon = L.Icon.extend({
    options: {
        iconSize:     [64, 64],
        iconAnchor:   [32, 64],
        popupAnchor:  [-3, -76]
    }
});

// creation du droupe d'icone
var myIconGroup = L.featureGroup().addTo(mymap).on("click", groupClick);


// declaration de l'objet regroupant l'icone, sa postion et le niveau de zoom
function obj(id, pos, icon, zoom, d) {
    this.id = id;
    this.pos = pos;
    this.icon = icon;
    this.zoom = zoom;
    this.description = d;
};

// conditions sur les objets, contient des indices des objets
var ObjRecuperable = [2,3, 4, 5, 6, 7, 8,9];     // de 1 a 10
var ObjBloqueParObj = [1];    //  1 
var ObjLibereObjBloque = [9, 10]; // de 9 a 10
var ObjChargeAuDepart = I;   // tous les objets sont chargees au depart


// Dans l'inventaire du joueur je recupere les id (niveau)
var inventaire = playerObject[0].inventaire;
var id_inventaire = [];
for (let i=0; i<inventaire.length; i++){
    let a = inventaire[i].niveau;
    id_inventaire.push(a);
    // suppression des indices deja dans l'inventaire
    if (a<=2){
        ObjBloqueParObj.splice(ObjBloqueParObj.indexOf(a), 1);
    }
    else if (a>=9){
        ObjLibereObjBloque.splice(ObjLibereObjBloque.indexOf(a), 1);
    }
}

console.log(ObjBloqueParObj, ObjLibereObjBloque);

// tableau d'objets non presents dans l'inventaire du joueur
var TabObj = [];
var invObj = [];
for (let i=0; i<I.length; i++){
    if (id_inventaire.indexOf(I[i]) == -1){
        TabObj.push(new obj(I[i], 
                        Positions[i],  
                        new LeafIcon({iconUrl: "Icons/"+IconURLs[i]}), 
                        Zooms[i], 
                        d[i]));
    }
    else {
        invObj.push(new obj(I[i], 
            Positions[i],  
            new LeafIcon({iconUrl: "Icons/"+IconURLs[i]}), 
            Zooms[i], 
            d[i]));
    }
};

// affichage des objets present dans l'inventaire des le depart
for (let i=0; i<invObj.length; i++){
    addToInventory(invObj[i]);
    let temp = ObjLibereObjBloque.indexOf(invObj[i].id);
    if (temp != -1){
        // on suprime cet objet des objets pouvant liberer un autre
        ObjLibereObjBloque.splice(temp, 1);
        // on supprime un objet de liste des objets bloques par un autre
        let t = ObjBloqueParObj.splice(0, 1);
        // ajoute l'objet debloque dans la liste des objets recuperables
        ObjRecuperable.push(t[0]);
    }
}


// ajout creation des markers a partir des objets 
function addToGroup(){
    var tab = [];
    for (let i=0; i<TabObj.length; i++){ 
        if ( TabObj[i].zoom < mymap.getZoom()){
            tab.push(L.marker(TabObj[i].pos, {icon: TabObj[i].icon}).addTo(myIconGroup).bindPopup(TabObj[i].description));
        }
        else {
            tab.push(L.marker(TabObj[i].pos, {icon: TabObj[i].icon}).bindPopup(TabObj[i].description));
        }
    } 
    return tab;   
}

// tableau de markers
var tabMarkers = addToGroup();

// fonction qui est declanche quand on clique sur un marqueur
function groupClick(event) {
    let j = tabMarkers.indexOf(event.layer);
    // si c'est un objet pouvant liberer un objet bloque
    if (ObjLibereObjBloque.indexOf(TabObj[j].id) != -1){
        liberer(event);
    }
    // si c'est un objet recuperable
    else if (ObjRecuperable.indexOf(TabObj[j].id) != -1 ){ 
        recuper(event);
    }
    // si c'est un objet bloque par un autre objet
    else if (ObjBloqueParObj.indexOf(TabObj[j].id) != -1){
        afficheBloqueObjet(event);
    }
    // si c'est un objet charge au demarrage du jeu, par defaut tous les objets se chargent au debut
    else if (ObjChargeAuDepart.indexOf(TabObj[j].id) != -1){
        // on ne fait rien
    }

  }

// fonction qui est declenchee quand on sur la carte
mymap.on('zoom' , function(e){
    //move_player(mymap.getCenter());
    for (let i=0; i< tabMarkers.length; i++){
        if ( TabObj[i].zoom > mymap.getZoom()) {
            if (myIconGroup.hasLayer(tabMarkers[i])){
                mymap.removeLayer(tabMarkers[i]);
                myIconGroup.removeLayer(tabMarkers[i]);
            }
        }
        else {
            if (!myIconGroup.hasLayer(tabMarkers[i])) {
                tabMarkers[i].addTo(myIconGroup);
            }
        }
    }
});


// fonction qui ajoute l'objet trouve dans l'inventaire du joueur
function addToInventory(item){
    let img = document.createElement("img");
    img.src = item.icon.options.iconUrl;
    let div = document.createElement("div");
    div.className = "grid-item";
    div.appendChild(img);
    document.getElementById('items').appendChild(div);
    if (id_inventaire.indexOf(item.id) == -1){   // si l'item n'est pas deja  dans l'inventaire
        addItem(item);
    }
}

// fonction qui execute l'action d'un objet recuperable
function recuper(event){
    mymap.removeLayer(event.layer);                  // suppression de la carte
    myIconGroup.removeLayer(event.layer);            // suppression du groupe de layers
    if (TabObj.length != 1){
        let j = tabMarkers.indexOf(event.layer);
        addToInventory(TabObj[j]);                   // ajout de l'objet dans l'inventaire du joueur
        TabObj.splice(j, 1);                         // suppression de cet objet
        tabMarkers.splice(j, 1);                     // suppression du marker
        return j,TabObj;
    }
    else {
        addToInventory(TabObj[0]);  
        TabObj.splice(0, 1);      
        tabMarkers.splice(0, 1);      
    }
}

// fonction qui s'execute sur un objet bloque par un autre
function afficheBloqueObjet(event){
    let j = tabMarkers.indexOf(event.layer);
    tabMarkers[j].bindPopup(TabObj[j].description);
    console.log("bloque par un autre objet");
}

// fonction qui s'execute lorsqu'un objet peut liberer un autre
function liberer(event){
    console.log("libere un objet");
    let j = tabMarkers.indexOf(event.layer);
    tabMarkers[j].bindPopup(TabObj[j].description);
    L.popup().setLatLng(TabObj[j].pos).setContent(TabObj[j].description).openOn(mymap);
    // on suprime cet objet des objets pouvant liberer un autre
    let k = ObjLibereObjBloque.indexOf(TabObj[j]);
    ObjLibereObjBloque.splice(k, 1);
    // on supprime un objet de liste des objets bloques par un autre
    let t = ObjBloqueParObj.splice(0, 1);
    // ajoute l'objet debloque dans la liste des objets recuperables
    ObjRecuperable.push(t[0]);
    recuper(event);
}




// ---------------------------------- AJAX ----------------------------------------------

// addItem
function gameover(){
    affiche_scenario(10);
    $(document).ready(function(event){
        $.ajax({
            url: "/endGameEmail",
            type: "POST",
            data: {"valid": 1},
            dataType: "json",
            async: true,
            success: function(data,status){console.log();}
        })
    });
    setTimeout(()=>{$('#redirect')[0].click(); },20000);

    
}
function addItem(item){
    $(document).ready(function(event){
        $.ajax({
            url: "/add_item",
            type: "POST",
            data: {"description": item.description,"visibility": item.zoom,"icon_id":item.id},
            dataType: "json",
            async: true,
            success: function(data,status){
                if(item.id==9){
                    $(document).ready(affiche_scenario(8)); 
                    setTimeout(()=>{$(document).ready(affiche_scenario(9)); },10000);
                }
                else if(item.id==8){
                    gameover()
                }
                else{
                affiche_scenario(item.id);
                }
            }
        })
    });
}

// move_player
function move_player(e){
    $(document).ready(function(event){
        $.ajax({
            url: "/move_player",
            type: "POST",
            data: {"longitude": e.latlng.lng,"latitude": e.latlng.lat},
            dataType: "json",
            async: true,
            success: function(data,status){
                /*mymap.panTo(new L.LatLng(data["latitude"], data["longitude"]));*/
                console.log();
            }
        })
    });
}

mymap.on('click', move_player);

// affiche_scenario
function affiche_scenario(num){
    $(document).ready(function(event){
        console.log(num);
        $.ajax({
            url: "/affiche_scenario",
            type: "POST",
            data: {"num_scenario": num},
            dataType: "json",
            async: true,
            success: function(data,status){
                var msg=data["msg"];
                if (num==0){
                    msg=msg+" "+playerObject[0].pseudo;
                }
                $("#msg").text(msg);
                $("#dialogbox").css("opacity","1.0");
                setTimeout(() => {  $("#dialogbox").css("opacity","0.0"); }, 10000);
            }
        })
    });
}
if(inventaire.length<1){
    //Présentation
    $(document).ready(affiche_scenario(11)); 
    setTimeout(()=>{$(document).ready(affiche_scenario(12)); },10000);
}
else if(inventaire.length>=8){
    gameover();
}
else{
    $(document).ready(affiche_scenario(0)); //Message de bienvenue
    setTimeout(()=>{$(document).ready(affiche_scenario(inventaire.length-1)); },10000);//On rapelle le dernier indice
    

}



