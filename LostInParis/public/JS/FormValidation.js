
var signup=document.getElementById("sign-up");
var signup_error=document.getElementById("signup_error");

function signupValid(){
    signup.pseudo.style.borderColor="white";
    signup.mail.style.borderColor="white";
    signup.pass.style.borderColor="white";
    signup.confirm_pass.style.borderColor="white";

    console.log("sign up validation");
    if(!signup.pseudo.value || (!signup.pass.value) || (!signup.mail.value) || (!signup.confirm_pass.value)){
        signup_error.textContent="Veuillez renseigner tout les champs !";
        return false;
    }
    else if(!(/^[a-zA-Z]/.test(signup.pseudo.value))){
        signup_error.textContent="Le premier caractère de votre id doit être une lettre!";
        signup.pseudo.style.borderColor="red";
        return false;
    }
 
    else if(!(/\S+@\S+\.\S+/.test(signup.mail.value))){
        signup_error.textContent="Veuillez saisir une adresse mail valide !";
        signup.mail.style.borderColor="red";
        return false;
    }
   
    else if (signup.confirm_pass.value!=signup.pass.value){
        signup_error.textContent="Veuillez confirmer votre mot de passe !";
        signup.pass.style.borderColor="red";
        signup.confirm_pass.style.borderColor="red";
        return false;
    }
    else{
        return true;
    }

}
 