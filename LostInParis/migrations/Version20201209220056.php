<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201209220056 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE players ADD username VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE players ADD password VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE players DROP pseudoname');
        $this->addSql('ALTER TABLE players DROP pass');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE players ADD pseudoname VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE players ADD pass VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE players DROP username');
        $this->addSql('ALTER TABLE players DROP password');
    }
}
