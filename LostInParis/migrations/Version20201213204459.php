<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201213204459 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE inventaire_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE items_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE lieux_mysteres_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE objects_icons_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE players_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE scenarios_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE inventaire (id INT NOT NULL, player_id_id INT NOT NULL, exist BOOLEAN DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_338920E0C036E511 ON inventaire (player_id_id)');
        $this->addSql('CREATE TABLE items (id INT NOT NULL, interaction_avec_lieux_id INT DEFAULT NULL, inventaire_id_id INT DEFAULT NULL, description TEXT NOT NULL, niveau INT DEFAULT NULL, visibility INT NOT NULL, longitude DOUBLE PRECISION DEFAULT NULL, latitude DOUBLE PRECISION DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_E11EE94D4035C78E ON items (interaction_avec_lieux_id)');
        $this->addSql('CREATE INDEX IDX_E11EE94D9485E435 ON items (inventaire_id_id)');
        $this->addSql('CREATE TABLE lieux_mysteres (id INT NOT NULL, longitude DOUBLE PRECISION NOT NULL, latitude DOUBLE PRECISION NOT NULL, altitude DOUBLE PRECISION DEFAULT NULL, indices TEXT NOT NULL, niveau INT NOT NULL, visibility INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE objects_icons (id INT NOT NULL, icon_path VARCHAR(255) NOT NULL, description TEXT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE players (id INT NOT NULL, username VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, email VARCHAR(255) DEFAULT NULL, elapsed_time TIME(0) WITHOUT TIME ZONE DEFAULT NULL, longitude DOUBLE PRECISION DEFAULT NULL, latitude DOUBLE PRECISION DEFAULT NULL, altitude DOUBLE PRECISION DEFAULT NULL, niveau INT DEFAULT NULL, score INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE scenarios (id INT NOT NULL, n_sc INT NOT NULL, description TEXT DEFAULT NULL, texte_a_raconter TEXT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE inventaire ADD CONSTRAINT FK_338920E0C036E511 FOREIGN KEY (player_id_id) REFERENCES players (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE items ADD CONSTRAINT FK_E11EE94D4035C78E FOREIGN KEY (interaction_avec_lieux_id) REFERENCES lieux_mysteres (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE items ADD CONSTRAINT FK_E11EE94D9485E435 FOREIGN KEY (inventaire_id_id) REFERENCES inventaire (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE items DROP CONSTRAINT FK_E11EE94D9485E435');
        $this->addSql('ALTER TABLE items DROP CONSTRAINT FK_E11EE94D4035C78E');
        $this->addSql('ALTER TABLE inventaire DROP CONSTRAINT FK_338920E0C036E511');
        $this->addSql('DROP SEQUENCE inventaire_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE items_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE lieux_mysteres_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE objects_icons_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE players_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE scenarios_id_seq CASCADE');
        $this->addSql('DROP TABLE inventaire');
        $this->addSql('DROP TABLE items');
        $this->addSql('DROP TABLE lieux_mysteres');
        $this->addSql('DROP TABLE objects_icons');
        $this->addSql('DROP TABLE players');
        $this->addSql('DROP TABLE scenarios');
    }
}
