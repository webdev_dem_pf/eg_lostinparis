<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201130152206 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE scenarios_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE scenarios (id INT NOT NULL, n_sc INT NOT NULL, description TEXT DEFAULT NULL, texte_a_raconter TEXT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE items ALTER niveau DROP NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE scenarios_id_seq CASCADE');
        $this->addSql('DROP TABLE scenarios');
        $this->addSql('ALTER TABLE items ALTER niveau SET NOT NULL');
    }
}
