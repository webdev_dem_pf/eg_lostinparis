<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201203135217 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE items DROP CONSTRAINT fk_e11ee94d9485e435');
        $this->addSql('DROP INDEX idx_e11ee94d9485e435');
        $this->addSql('ALTER TABLE items DROP inventaire_id_id');
        $this->addSql('ALTER TABLE players ALTER elapsed_time DROP NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE players ALTER elapsed_time SET NOT NULL');
        $this->addSql('ALTER TABLE items ADD inventaire_id_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE items ADD CONSTRAINT fk_e11ee94d9485e435 FOREIGN KEY (inventaire_id_id) REFERENCES inventaire (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_e11ee94d9485e435 ON items (inventaire_id_id)');
    }
}
