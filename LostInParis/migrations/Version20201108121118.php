<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201108121118 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE items_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE lieux_mysteres_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE objects_icons_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE players_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE items (id INT NOT NULL, interaction_avec_lieux_id INT DEFAULT NULL, description TEXT NOT NULL, niveau INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_E11EE94D4035C78E ON items (interaction_avec_lieux_id)');
        $this->addSql('CREATE TABLE lieux_mysteres (id INT NOT NULL, longitude DOUBLE PRECISION NOT NULL, latitude DOUBLE PRECISION NOT NULL, altitude DOUBLE PRECISION DEFAULT NULL, indices TEXT NOT NULL, niveau INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE objects_icons (id INT NOT NULL, icon_path VARCHAR(255) NOT NULL, description TEXT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE players (id INT NOT NULL, pseudoname VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE items ADD CONSTRAINT FK_E11EE94D4035C78E FOREIGN KEY (interaction_avec_lieux_id) REFERENCES lieux_mysteres (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE items DROP CONSTRAINT FK_E11EE94D4035C78E');
        $this->addSql('DROP SEQUENCE items_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE lieux_mysteres_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE objects_icons_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE players_id_seq CASCADE');
        $this->addSql('DROP TABLE items');
        $this->addSql('DROP TABLE lieux_mysteres');
        $this->addSql('DROP TABLE objects_icons');
        $this->addSql('DROP TABLE players');
    }
}
