<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201123215935 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE inventaire_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE inventaire (id INT NOT NULL, player_id_id INT NOT NULL, exist BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_338920E0C036E511 ON inventaire (player_id_id)');
        $this->addSql('ALTER TABLE inventaire ADD CONSTRAINT FK_338920E0C036E511 FOREIGN KEY (player_id_id) REFERENCES players (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE items ADD inventaire_id_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE items ADD visibility INT NOT NULL');
        $this->addSql('ALTER TABLE items ADD longitude DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE items ADD latitude DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE items ADD CONSTRAINT FK_E11EE94D9485E435 FOREIGN KEY (inventaire_id_id) REFERENCES inventaire (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_E11EE94D9485E435 ON items (inventaire_id_id)');
        $this->addSql('ALTER TABLE lieux_mysteres ADD visibility INT NOT NULL');
        $this->addSql('ALTER TABLE players ADD pass VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE players ADD email VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE players ADD elapsed_time TIME(0) WITHOUT TIME ZONE NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE items DROP CONSTRAINT FK_E11EE94D9485E435');
        $this->addSql('DROP SEQUENCE inventaire_id_seq CASCADE');
        $this->addSql('DROP TABLE inventaire');
        $this->addSql('ALTER TABLE players DROP pass');
        $this->addSql('ALTER TABLE players DROP email');
        $this->addSql('ALTER TABLE players DROP elapsed_time');
        $this->addSql('ALTER TABLE lieux_mysteres DROP visibility');
        $this->addSql('DROP INDEX IDX_E11EE94D9485E435');
        $this->addSql('ALTER TABLE items DROP inventaire_id_id');
        $this->addSql('ALTER TABLE items DROP visibility');
        $this->addSql('ALTER TABLE items DROP longitude');
        $this->addSql('ALTER TABLE items DROP latitude');
    }
}
