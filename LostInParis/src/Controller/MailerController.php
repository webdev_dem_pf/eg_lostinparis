<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;

class MailerController extends AbstractController
{
    /**
     * @Route("/mailer", name="mailer")
     */
    public function index(): Response
    {
        return $this->render('mailer/index.html.twig', [
            'controller_name' => 'MailerController',
        ]);
    }
    private $mailer;
    public function __construct(MailerInterface $mailer){
        $this->mailer=$mailer;
    }
    /**
     * @Route("/email")
     */
    public function sendEmail(string $useremail,string $username,MailerInterface $mailer,string $template,string $subject)
    {
        $email = (new TemplatedEmail())
            ->from('lostinparis@mail.com')
            ->to($useremail)
            ->subject($subject)
            ->text("Regardez le template html!")
            ->htmlTemplate($template)
            ->context(["username"=>$username]);

        $mailer->send($email);
    }
}
