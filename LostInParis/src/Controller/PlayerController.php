<?php

namespace App\Controller;
use App\Entity\Items;
use App\Entity\Players;
use App\Entity\Scenarios;
use App\Entity\Inventaire;
use App\Controller\MailerController;
use App\Security\TokenAuthenticator;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Mailer\MailerInterface;


class PlayerController extends AbstractController
{
    private $em;

    public function __construct(EntityManagerInterface $entityManager)
    {
        
        $this->em = $entityManager;
    }
    /**
     * @Route("/player", name="player")
     */
    public function index(): Response
    {
        return $this->render('player/index.html.twig', [
            'controller_name' => 'PlayerController',
        ]);
    }
    /**
     * @Route("/login_Player", name="login_Player")
     */
    public function loginPlayer(){
        //$error = $authenticationUtils->getLastAuthenticationError();
        return $this->render("app/player_interface.html.twig");//,["error"=>$error]);
    }
    /**
     * @Route("/redirect_player", name="redirect_player")
     */
    public function redirectPlayer(){
        $player = $this->getUser();
        if($player){
            $inventaire=[];
            foreach($player->getInventaire()->getItemId() as $item){
                $inventaire[]=$item;
            }
            if(sizeof($inventaire)>=8){
            return $this->render('mailer/GameOver.html.twig',['username'=>$player->getUsername()]);
            }
            else{
            return $this->render("app/player_interface.html.twig",['player'=>$player,'inventaire'=>$inventaire]);
            }
        }
        else{
            $repository = $this->getDoctrine()->getRepository(Players::class);
            $top_players = $repository->findBy(array(), array('elapsed_time' => 'DESC'),10);
            return $this->render("app/home.html.twig",["top_players"=>$top_players]);
        }
        
    }

    /**
     * @Route("/logout_Player", name="logout_Player")
     */
    public function logoutPlayer(){
        return $this->render("app/home.html.twig");
    }
    /**
     * @Route("/create_interface", name="create_Player")
     */
    public function createPlayer(Request $request,EntityManagerInterface $em,UserPasswordEncoderInterface $encoder,GuardAuthenticatorHandler $guardHandler,TokenAuthenticator $authenticator,MailerController $mailer,MailerInterface $emailer): Response
    {
        $repository = $this->getDoctrine()->getRepository(Players::class);
        if($request->request->count()>0){
            if(($repository->findBy(["username"=>$request->request->get("pseudo")])) || ($repository->findBy(["email"=>$request->request->get("mail")]))){
                $top_players = $repository->findBy(array(), array('elapsed_time' => 'DESC'),10);
                return $this->render("app/home.html.twig",["msg"=>"Username ou mail déja utilisé!","top_players"=>$top_players]);

            }
            else{
                $coord=[48.8413672,2.4223428];
                $player = new Players();
                $player->setUsername($request->request->get("pseudo"))
                       ->setPassword($request->request->get("pass"))
                       ->setEmail($request->request->get("mail"))
                       ->setElapsedTime(new \DateTime('@'.strtotime('now')));
                $player->setLongitude($coord[1]);
                $player->setLatitude($coord[0]);
                $inventaire=new Inventaire();
                $inventaire->setPlayerId($player);
                $inventaire->setExist(true);
                $player->setInventaire($inventaire);
                $this->em=$em;
                $hash=$encoder->encodePassword($player, $player->getPassword());
                $player->setPassword($hash);
                $em->persist($player);
                $em->persist($inventaire);
                $mailer->sendEmail($player->getEmail(),$player->getUsername(),$emailer,'mailer/SignupConfirmation.html.twig',"LostInParis confirmation d'inscription!");

                $em->flush();
                return $guardHandler->authenticateUserAndHandleSuccess($player,$request,$authenticator,'main');
                
            }

        }

    }

    /**
     * @Route("/game_items", name="game_items")
     */
    public function game_items(Request $request){
        if ($request->isXmlHttpRequest()){
            $repository = $this->getDoctrine()->getRepository(Items::class);
            $gameitems = $repository->findBy(array(), array('id' => 'DESC'),9);
            $gameitems2=[];
            foreach($gameitems as $item){
                $gameitems2[]=array("id"=>$item->getId(),"longitude"=>$item->getLongitude(),"latitude"=>$item->getLatitude(),"visibility"=>$item->getVisibility(),"niveau"=>$item->getNiveau(),"description"=>$item->getDescription());
            }
            return new JsonResponse($gameitems2);

        }
        else{
            return new JsonResponse(array(["error"=>"Something went wrong!"]));
        }
    }

    /**
     * @Route("/add_item",name="add_item")
     */
    public function addItem(Request $request,EntityManagerInterface $em){
        
        if ($request->isXmlHttpRequest()){
            $this->em=$em;
            $player = $this->getUser();
            $inventaire=$player->getInventaire();
            $item=new Items();
            $item->setDescription($request->request->get("description"))
                 ->setVisibility($request->request->get("visibility"))
                 ->setNiveau($request->request->get("icon_id"));
            $em->persist($item);
            $inventaire->addItemId($item);
            $em->flush();
            return new JsonResponse(array("username"=>$player->getUsername()));
        }
        else{
            return new JsonResponse(array(["error"=>"Something went wrong!"]));
        }
        
        
    }

    /**
     * @Route("/move_player",name="move_player")
     */
    public function moveplayer(Request $request,EntityManagerInterface $em){
        
        if ($request->isXmlHttpRequest()){
            $this->em=$em;
            $player = $this->getUser();
            $player->setLongitude($request->request->get("longitude"))
                   ->setLatitude($request->request->get("latitude"));
            $em->flush();
            return new JsonResponse(array("username"=>$player->getUsername(),"longitude"=>$player->getLongitude(),"latitude"=>$player->getLatitude()));
        }
        else{
            return new JsonResponse(array(["error"=>"Something went wrong!"]));
        }
        

    }
    /**
     * @Route("/affiche_scenario", name="affiche_scenario")
     */
    public function afficheScenario(Request $request){
        if ($request->isXmlHttpRequest()){
            $repository = $this->getDoctrine()->getRepository(Scenarios::class);
            $sc=$repository->findOneBy(["N_sc"=>$request->request->get("num_scenario")]);
            return new JsonResponse(array("msg"=>$sc->getTexteARaconter()));

        }
        else{
            return new JsonResponse(array(["error"=>"Something went wrong!"]));
        }
    }
    /**
     * @Route("/endGameEmail", name="endGameEmail")
     */
    public function endGameEmail(Request $request,MailerController $mailer,MailerInterface $emailer){
        if ($request->isXmlHttpRequest()){
            $player = $this->getUser();
            $mailer->sendEmail($player->getEmail(),$player->getUsername(),$emailer,'mailer/GameOver.html.twig',"LostInParis Game Over!");
            return new JsonResponse(array("msg"=>"Email sent!"));

        }
        else{
            return new JsonResponse(array(["error"=>"Something went wrong!"]));
        }
    }
}
?>

