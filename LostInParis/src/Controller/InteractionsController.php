<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Items;
use App\Entity\Players;
use App\Entity\Inventaire;
use Symfony\Component\HttpFoundation\JsonResponse;


class InteractionsController extends AbstractController
{
    /**
     * @Route("/interactions", name="interactions")
     */
    public function index(): Response
    {
        return $this->render('interactions/index.html.twig', [
            'controller_name' => 'InteractionsController',
        ]);
    }
    /**
     * @Route("/interactions/test1",name="test1")
     */
    public function moveplayer(Request $request){
        if ($request->isXmlHttpRequest()){
            $this->em=$em;
            $player = $this->getUser();
            $player->setLongitude($request->request->get("longitude"))
                   ->setLatitude($request->request->get("latitude"));
            $em->flush();
            return new JsonResponse(array("player"=>$player));
        }
        else{
            return new JsonResponse(array(["error"=>"Something went wrong!"]));
        }
        
    }


    /**
     * @Route("/interactions/test2", name="test2")
     */
    public function afficheScenario(Request $request){
        if ($request->isXmlHttpRequest()){
            $repository = $this->getDoctrine()->getRepository(Scenarios::class);
            return new JsonResponse($repository->findBy($request->request->get("num_scenario")));
        }
        else{
            return new JsonResponse(array(["error"=>"Something went wrong!"]));

        }
    }


    /**
     * @Route("/interactions/test3",name="test3")
     */
    public function addItem(Request $request,EntityManagerInterface $em){
        /*
        if ($request->isXmlHttpRequest()){
            $this->em=$em;
            $player = $this->getUser();
            $inventaire=$player->getInventaire();
            $item=new Items();
            $item->setDescription($request->request->get("description"))
                 ->setVisibility($request->request->get("visibility"))
                 ->setNiveau($request->request->get("icon_id"));
            $em->persist($item);
            $inventaire->addItemId($item);
            $em->flush();
            return new JsonResponse(array("player"=>$player,"inventaire"=>$inventaire->getItem_id()));
        }
        else{
            return new JsonResponse(array["error"=>"Something went wrong!"]);
        }*/
        $player = $this->getUser();
        $inventaire=$player->getInventaire();
        return new JsonResponse(array("player"=>$player,"inventaire"=>$inventaire->getItem_id()));
        
    }
}