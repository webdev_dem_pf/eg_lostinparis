<?php

namespace App\Controller;

use App\Entity\Players;
use App\Entity\Inventaire;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


class AppController extends AbstractController
{

    /**
     * @Route("/app", name="app")
     */
    public function index(): Response
    {
        return $this->render('app/index.html.twig', [
            'controller_name' => 'AppController',
        ]);
    }
    /**
     * @Route("/", name="home")
     */
    public function home(){
        $repository = $this->getDoctrine()->getRepository(Players::class);
        $top_players = $repository->findBy(array(), array('elapsed_time' => 'DESC'),10);
        return $this->render('app/home.html.twig',["top_players"=>$top_players]);
    }

}
