<?php

namespace App\Entity;

use App\Repository\ItemsRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ItemsRepository::class)
 */
class Items
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $niveau;

    /**
     * @ORM\OneToOne(targetEntity=LieuxMysteres::class, cascade={"persist", "remove"})
     */
    private $interaction_avec_lieux;

    /**
     * @ORM\Column(type="integer")
     */
    private $visibility;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $longitude;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $latitude;
     /**
     * @ORM\ManyToOne(targetEntity=Inventaire::class, inversedBy="item_id")
     */
    private $inventaire_id;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getNiveau(): ?int
    {
        return $this->niveau;
    }

    public function setNiveau(int $niveau): self
    {
        $this->niveau = $niveau;

        return $this;
    }
    
   
    public function getInventaireId(): ?Inventaire
    {
        return $this->inventaire_id;
    }

    public function setInventaireId(?Inventaire $inventaire_id): self
    {
        $this->inventaire_id = $inventaire_id;

        return $this;
    }

    public function getInteractionAvecLieux(): ?LieuxMysteres
    {
        return $this->interaction_avec_lieux;
    }

    public function setInteractionAvecLieux(?LieuxMysteres $interaction_avec_lieux): self
    {
        $this->interaction_avec_lieux = $interaction_avec_lieux;

        return $this;
    }



    public function getVisibility(): ?int
    {
        return $this->visibility;
    }

    public function setVisibility(int $visibility): self
    {
        $this->visibility = $visibility;

        return $this;
    }

    public function getLongitude(): ?float
    {
        return $this->longitude;
    }

    public function setLongitude(?float $longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }

    public function getLatitude(): ?float
    {
        return $this->latitude;
    }

    public function setLatitude(float $latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }
}
