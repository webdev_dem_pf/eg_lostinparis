<?php

namespace App\Entity;

use App\Repository\InventaireRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=InventaireRepository::class)
 */
class Inventaire
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity=Items::class, mappedBy="inventaire_id")
     */
    private $item_id;



    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $exist;

    /**
     * @ORM\OneToOne(targetEntity=Players::class, inversedBy="inventaire", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $player_id;

    public function __construct()
    {
        $this->item_id = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|Items[]
     */
    public function getItemId(): Collection
    {
        return $this->item_id;
    }

    public function addItemId(Items $itemId): self
    {
        if (!$this->item_id->contains($itemId)) {
            $this->item_id[] = $itemId;
            $itemId->setInventaireId($this);
        }

        return $this;
    }

    public function removeItemId(Items $itemId): self
    {
        if ($this->item_id->removeElement($itemId)) {
            // set the owning side to null (unless already changed)
            if ($itemId->getInventaireId() === $this) {
                $itemId->setInventaireId(null);
            }
        }

        return $this;
    }

    public function getPlayerId(): ?Players
    {
        return $this->player_id;
    }

    public function setPlayerId(Players $player_id): self
    {
        $this->player_id = $player_id;

        return $this;
    }

    public function getExist(): ?bool
    {
        return $this->exist;
    }

    public function setExist(bool $exist): self
    {
        $this->exist = $exist;

        return $this;
    }
}
