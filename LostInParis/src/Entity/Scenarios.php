<?php

namespace App\Entity;

use App\Repository\ScenariosRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ScenariosRepository::class)
 */
class Scenarios
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $N_sc;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $texte_a_raconter;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNSc(): ?int
    {
        return $this->N_sc;
    }

    public function setNSc(int $N_sc): self
    {
        $this->N_sc = $N_sc;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getTexteARaconter(): ?string
    {
        return $this->texte_a_raconter;
    }

    public function setTexteARaconter(?string $texte_a_raconter): self
    {
        $this->texte_a_raconter = $texte_a_raconter;

        return $this;
    }
}
