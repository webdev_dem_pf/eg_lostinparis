<?php

namespace App\DataFixtures;

use App\Entity\Items;
use App\Entity\Players;
use App\Entity\Scenarios;
use App\Entity\Inventaire;
use App\Entity\ObjectsIcons;
use App\Entity\LieuxMysteres;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;




class InitGame extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $descriptions=["Symbole ancestrale de la foudre est l’arme et l’attribut du dieu céleste.",
        "La lumière de l’aurore, symbole ancestrale de feu, éclaire les ténèbres.",
        "Le sceau de la destruction, symbole ancestrale de chaos, est invoque pour destruction.",
        "Orcus, symbole ancestrale de ténèbres, est le gardien du portail dimensionnel qui lie le monde des vivants a celui des morts.",
        "Le trou de verre, symbole ancestrale de l’espace, est le portail vers les mondes parallèles.",
        "La légende dit que derrière le masque démoniaque, symbole ancestrale dévorant, se cache l’esprit du Fondateur universel.",
        "Le sceau mortel, symbole ancestrale de mortalité, est la porte vers le monde des morts.",
        "Le cristal céleste, symbole ancestrale de glace, d’une pureté incomparable, garde la mémoire de tout être.",
        "Carte IGN"
        ];
        
        $longitudes=[2.2980982265261214, 2.2950382263365174, 2.3498806398297005, 2.3323045912124822, 2.3219943128411806, 2.337708370512593, 2.393483226336019, 2.2797002155882997, 2.424728420891207];
        $latitudes=[48.85601858369315,48.873996319362206,48.85308819073639,48.83393293344259,48.84229322175888,48.860738130825304,48.86163432006842,48.8500663177625,48.84571459775079 ];
        $visibility=10;
        $game_master=new Players();
        $game_master->setUsername("GameMaster")
                ->setPassword("0000")
                ->setEmail("gamemaster@mail.com")
                ->setElapsedTime(new \DateTime('@'.strtotime('now')));
        $inventaire=new Inventaire();
        $inventaire->setPlayerId($game_master);
        $item=new Items();
        $item->setDescription("trash")
             ->setVisibility(0)
             ->setLongitude(0)
             ->setLatitude(0);
        $inventaire->addItemId($item);
        $manager->persist($item);
        

        for($i=0;$i<9;$i++){
            $item=new Items();
            $item->setDescription($descriptions[$i])
                 ->setVisibility($visibility)
                 ->setLongitude($longitudes[$i])
                 ->setLatitude($latitudes[$i]);
            $inventaire->addItemId($item);
            $manager->persist($item);
        }
        $game_master->setInventaire($inventaire);
        $manager->persist($game_master);
        $manager->persist($inventaire);
        
        $destination_finale=new LieuxMysteres();
        $destination_finale->setLongitude(2.120333939827709)
                           ->setLatitude(48.80495674091527)
                           ->setIndices("")
                           ->setVisibility($visibility)
                           ->setNiveau(10);
        $manager->persist($destination_finale);
        $texte_scenarios=["Notre aventure reprend",
        "Lumière de l’aurore procure un feu éternel",
        "Le Sceau de la destruction a certainement causé une catastrophe lors de son arrivé sur cette terre, souvent il cause des incendies!",
        "Le symbole ancestrale des ténèbres Orcus sera certainement dans un endroit sous terrain à l’abri des lumières et loin de toute signe de vie",
        "Le trou de verre réside toujours dans les lieux les plus hauts",
        "Le masque déomniaque, symbole ancestrale dévorant, est attiré par les lieux ou il existe une énergie spirituel à dévorer, ce genre d'énergie existe
        souvent dans les anciens trésors",
        "Le sceau mortel, absorbe l’énergie spirituel des morts.",
        "Le cristal céleste sera certainement dans un endrit isolé, entouré d’eau et devant un symbole de liberté",
        "Bien maintenant on peut naviguer et trouver les endroits suceptibles d’héberger les symboles ancestraux",
        "Le symbole ancestrale de la foudre est attiré par les structures métalliques! Je crois que je sais ou je devrais le chercher",
        "Super, nous avons réussies à récuperer tout les huits symboles ancestraux! Rentrons à la Royaume Immortel!",
        "Oh je suis dans un nouveau monde! Je devrais trouver un moyen qui me permet d’avoir des renseignements sur la géographie de cette terre étrange",
        "Mes compétences magiques me disent qu’il y a un endroit qui s’appelle IGN à côté ou il existe des informations géographiques et
         topographiques précises! Infiltrons nous dedans"];
        for($i=0;$i<sizeof($texte_scenarios);$i++){
            $scenario=new Scenarios();
            $scenario->setTexteARaconter($texte_scenarios[$i])
                 ->setNSc($i);
            $manager->persist($scenario);
        }
        $manager->flush();
    }
}
