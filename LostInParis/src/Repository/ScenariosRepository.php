<?php

namespace App\Repository;

use App\Entity\Scenarios;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Scenarios|null find($id, $lockMode = null, $lockVersion = null)
 * @method Scenarios|null findOneBy(array $criteria, array $orderBy = null)
 * @method Scenarios[]    findAll()
 * @method Scenarios[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ScenariosRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Scenarios::class);
    }

    // /**
    //  * @return Scenarios[] Returns an array of Scenarios objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Scenarios
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
