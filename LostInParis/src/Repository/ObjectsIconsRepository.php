<?php

namespace App\Repository;

use App\Entity\ObjectsIcons;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ObjectsIcons|null find($id, $lockMode = null, $lockVersion = null)
 * @method ObjectsIcons|null findOneBy(array $criteria, array $orderBy = null)
 * @method ObjectsIcons[]    findAll()
 * @method ObjectsIcons[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ObjectsIconsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ObjectsIcons::class);
    }

    // /**
    //  * @return ObjectsIcons[] Returns an array of ObjectsIcons objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ObjectsIcons
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
