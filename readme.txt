The "Symfony LostInParis Application" is an escape game built using Symfony and leaflet JS

    PHP 7.2.9 or higher;
    PDO-PostgreSql PHP extension enabled;
    and the usual Symfony application requirements.

***Installation***
Download source code from git:https://gitlab.com/webdev_dem_pf/eg_lostinparis.git
Download and install composer on your computer and then run these command:
Go to the downloaded folder, open a terminal and run:

cd LostInParis
composer install

Now it's time to create the database, after configuring the .env file with the paramaters of your postgresql server:
postgresql://db_user:db_password@127.0.0.1:5432/db_name?serverVersion=11&charset=utf8 (put your own db_user and db_password, by default they are set to postgres)
Once it's done run the corresponding commands:

For Database creation: php bin/console doctrine:database:create
For Migrations from Objects entities to tables:php bin/console doctrine:migrations:migrate (if you see an error while executing this command ignore it and continue)
For Initializing database:php bin/console doctrine:fixtures:load (here you need to confirm the command by typing "yes", by default it's "no" so make sure you type "yes")
And that's it!Now you can start the loal server by running:php bin/console server:run


Then access the application in your browser at the given URL (https://localhost:8000 by default).

***Application architecture***
Our application , as all Symfony applications,use the three level Model Vue Template architecture.
***Technologies***
Our application isn't entirely php and twig file, in it's core it's a dynamic app JS and Jquery has been used to make that happen (to interact and modify html elements
on the map and also on the player inventory, when window size change, ajax queries etc) and also CSS to have a good looking rendering
You can find JS files in the following folder: public/JS
You can find CSS files in the following folder: public/css
You can find visual content such as images and icons in the following folder: public/Icons
***Services***
To offer the best user experience, our application has many embeded services, such as:
-Token Authentification service: this will provide the basic security requirements of every modern web app (token authentification,
hashed passwords,sql injection protection), check the config/packages/security.yaml and the src/security to check the details, the player controller call these
services when performing user registration, logging in and logging out
-Mailer service: this service allow our application to send emails to the users when register (to confirm if they really are the email owners), when they end the game
and it's also usefull to send updates news to the users of the app.See config/packages/mailer.yaml src/Controller/MailerController and src/templates/mailer
Do not hesitate and add a real email adress to test it.
***Data Model***
You can find the different classes/entities that make our app in the following folder:src/Entity
Legal mentions:
2020 Copyright ©  ENSG Géomatique.This application has been developped by Maghraoui Dhia Eddine & Namekong Teulong Patrick Franck All rights reserved.
All the icons used in our website are downloaded from:https://icones8.fr/
